#/bin/bash

######## Package installation ##

# Used to join domain
yum install krb5-workstation samba-common-tools sssd-ad oddjob-mkhomedir -y


# Radius
yum -y install make gcc pam pam-devel
wget ftp://ftp.freeradius.org/pub/radius/pam_radius-release_2_0_0.tar.gz


# Google authenticator
yum install epel-release -y
yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm -y
yum install google-authenticator qrencode-libs -y


####### Configuration ##

# Kerberos
cat ./kerberos_conf > /etc/krb5.conf

# Samba
cat ./samba_conf > /etc/smb.conf

# Radius
tar -xzvf pam-radius-release_2_0_0.tar.gz
cd pam-radius-release_2_0_0
./configure
make

cp pam_radius_auth.so /lib64/security/
mkdir /etc/raddb
cp pam_radius_auth.conf /etc/raddb/server

sed -i 's/#127.0.0.1/10.9.0.10               azerty                  3/g' /etc/raddb/server

# Google authenticator 
google-authenticator -s ~/.ssh/google_authenticator

# ssh
sed -i 's/#ChallengeResponseAuthentication no/ChallengeResponseAuthentication yes/g' /etc/ssh/sshd_config

# pam
sed -i 's/auth       include      system-auth/auth       required      pam_radius_auth.so/g' /etc/pam.d/sudo

sed -i '2s/^/auth    requisite    pam_radius_auth.so\n/' /etc/pam.d/sshd
sed -i '5s/^/auth      required     pam_google_authenticator.so\n/' /etc/pam.d/sshd


######## Network configuration ##

# Down NAT interface
nmcli connection down enp0s3
nmcli connection modify enp0s3 connection.autoconnect no

# Host configuration 
nmcli connection modify enp0s8 ipv4.addresses 10.5.0.10/24 connection.autoconnect yes ipv4.method manual type ethernet ipv4.dns 10.9.0.10 ipv4.dns-search projet.infra

nmcli connection reload 
nmcli connection down enp0s8
nmcli connection up enp0s8


####### Join Domain ##

# Domain
net ads join projet.infra --no-dns-updates -U arnaud

# Auto-configuration sssd
authconfig --update --enablesssd --enablesssdauth --enablemkhomedir

cat ./sssd_conf > /etc/sssd/sssd.conf
chown root:root /etc/sssd/sssd.conf
chmod 600 /etc/sssd/sssd.conf

systemctl restart sssd

# Connection with user's domain
su - arnaud@projet.infra

