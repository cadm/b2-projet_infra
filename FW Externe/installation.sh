#/bin/bash

####### Services installation ##

# Installation Iptables

systemctl stop firewalld
systemctl disable firewalld
systemctl mask --now firewalld

yum install -y nftables
systemctl start nftables
systemctl enable nftables


######## Network configuration ##


# Inter - FW
nmcli connection modify enp0s8 ipv4.addresses 10.0.15.2/30 ipv4.gateway 10.0.15.1 connection.autoconnect yes ipv4.method manual type ethernet

nmcli connection reload 
nmcli connection down enp0s8 
nmcli connection up enp0s8

# DMZ
nmcli connection add enp0s9 ipv4.addresses 10.0.1.1/24 connection.autoconnect yes ipv4.method manual type ethernet

nmcli connection reload 
nmcli connection down enp0s9
nmcli connection up enp0s9


